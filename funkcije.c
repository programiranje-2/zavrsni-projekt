#define _CRT_SECURE_NO_WARNINGS
#define _CRT_SECURE_NO_DEPRECATE  
#define _CRT_NONSTDC_NO_DEPRECATE

//Organizacija izvornog k�da.


#include<stdio.h>
#include<stdlib.h>
#include<string.h>                  //Ako su funkcije jednostavne koristiti makro funkcije ili inline funkcije.
#include<windows.h>
#include "Header.h"

struct manga {
	char ime[100];
	char autor[100];
	char zanr[100];
	int cijena;
} MANGA;

FILE* fptr;

typedef struct Manga {
	char ime[100];
	char autor[100];              //Primjena typedef sa strukturama i unijama, po potrebi s enum tipovima podataka tamo gdje treba.
	char zanr[100];
	int cijena;
} Manga;




//Generalno upotreba struktura i funkcija.




//FUNKCIJE ZA KUPCE

void mangaKupnja() {          //Imenovanje identifikatora (varijabli, konstanti, polja, funkcija, pokaziva�a�)
	                          //. Koristiti dinami�ko zauzimanje memorije za bilo koji tip podatka, osobito za slo�ene tipove podataka.
	                            //Koristiti funkcije malloc(), calloc(), realloc(), free() � neku od njih, ako ne i sve.
	system("color 0");
	mangatag(0);
	customerPage();


	FILE* fptr = fopen("mangashop.txt", "rb");          //Datoteke, koristiti tekstualnu ili binarnu, provjera pokaziva�a i zatvaranje datoteke.
	if (fptr == NULL) {
		printf("Greka pri otvaranju datoteke.\n");
		return;
	}


	Manga* manga = NULL;
	int numManga = 0;
	char valuta[] = "EUR";

	puts("Dostupne mange:\n\n");
	rewind(fptr);

	while (fread(&MANGA, sizeof(Manga), 1, fptr) == 1) {

		numManga++;
		manga = (Manga*)realloc(manga, numManga * sizeof(Manga));         //Generalno upotreba pokaziva�a tamo gdje su potrebni.

		strcpy(manga[numManga - 1].ime, MANGA.ime);
		strcpy(manga[numManga - 1].autor, MANGA.autor);
		strcpy(manga[numManga - 1].zanr, MANGA.zanr);
		manga[numManga - 1].cijena = MANGA.cijena;


		printf("Ime mange: %s\n", manga[numManga - 1].ime);
		printf("Autor: %s\n", manga[numManga - 1].autor);
		printf("Zanr: %s\n", manga[numManga - 1].zanr);
		printf("Cijena: %d ", manga[numManga - 1].cijena);
		printf("%s\n\n", valuta);
	}
	fclose(fptr);

	puts("Pritisnite ENTER kako bi nastavili kupnju...");
	getchar();
	mangatag(0);
	fflush(stdin);


	char mangaName[100];
	printf("Unesite ime mange koju zelite kupiti (Type 'RETURN' to cancel): ");
	fgets(mangaName, sizeof(mangaName), stdin);

	mangaName[strcspn(mangaName, "\n")] = '\0';

	mangatag(0);

	int found = 0;

	for (int i = 0; i < numManga; i++) {
		if (strcmpi(manga[i].ime, mangaName) == 0) {
			found = 1;

			mangapur(manga[i].cijena);

			printf("\n-------------------------------------------------\n");
			printf("\t\t VAS ODABIR:\n");
			printf("-------------------------------------------------\n\n");
			printf("Ime mange: %s\n", manga[i].ime);
			printf("Autor: %s\n", manga[i].autor);
			printf("Zanr: %s\n", manga[i].zanr);
			printf("Cijena: %d ", manga[i].cijena);
			printf("%s\n\n", valuta);
			printf("\t\t\tRACUN: %d ", manga[i].cijena);
			printf("%s\n\n", valuta);
			printf("Pritisnite '1' za povratak na glavnu stranicu, '2' za izlazak iz programa: ");

			char afterpurchase = getchar();

			if (afterpurchase == '1') {
				free(manga);
				main();
			}
			else {
				mangatag(0);
				system("color B1");
				printf("\n\n\n\n\n\n\t\t\t\tHVALA VAM ZA KUPNJU U NASEM SHOPU!\n");
				free(manga);
				getchar();
				exit(0);
			}
		}
	}

	if (!found && strcmpi(mangaName, "RETURN") != 0) {
		printf("\nNije dostupno!\n");
		printf("Pritisnite ENTER za nastavak!\n");
		getchar();
		mangaKupnja();
	}
	else if (strcmpi(mangaName, "RETURN") == 0) {
		free(manga);
		main();
	}

	free(manga);              //Sigurno brisanje memorije koja je dinami�ki zauzeta, anuliranje memorijskog prostora, provjera pokaziva�a kako se ne bi dogodila pogre�ka double free() i anuliranje svih pokaziva�a koji su bili usmjereni na memorijski prostor koji se dinami�ki zauzeo.
}




//FUNKCIJE ZA ADMINA


void adminLogin() {

	system("color 0");

	int izbor1;
	fptr = fopen("mangashop.txt", "ab+");
	fseek(fptr, 0, 0);
	mangatag(0);

	adminPage();
	printf("1. Dodaj novu mangu.");
	printf("\n2. Pregled svih dostupnih proizvoda.");
	printf("\n3. Uredi proizvod.");
	printf("\n4. Pretrazi mangu.");
	printf("\n5. Obrisi mangu.");
	printf("\n6. Povratak na prethodnu stranicu.\n");


	printf("Unesi odabir: ");
	scanf("%d", &izbor1);


	switch (izbor1) {
	case 1: dodaj();
	case 2: pregled();
	case 3: uredi();
	case 4: pretraga();
	case 5: obrisi();
	case 6: main();
	}
}






void dodaj() {              


	char id[100];             //Koristiti stati�ki zauzeta polja.
	mangatag(0);


	printf("\n\t\t     ***** Proizvodi: Dodatak kolekciji *****    ");
	fflush(stdin);
	adminPage();


	while (fread(&MANGA, sizeof(MANGA), 1, fptr) == 1) {

		if ((strcmpi(MANGA.ime, id)) == 0)
		{
			ERROR;
			printf("\nOprostite! Ova manga vec postoji!\n");
			printf("\nPritisnite ENTER kako bi dodali novi proizvod...\n");
			getch();
			dodaj();
		}
	}

	strcpy(MANGA.ime, id);

	printf("\nIme mange: ");
	gets(id);
	gets(MANGA.ime);

	printf("\nAutor: ");
	gets(MANGA.autor);

	printf("\nZanr: ");
	gets(MANGA.zanr);

	printf("\nCijena: ");
	scanf("%d", &MANGA.cijena);

	fwrite(&MANGA, sizeof(MANGA), 1, fptr);

	printf("\nPritisnite ENTER kako bi se vratili na pocetni ekran...\n");
	getch();

	system("cls");
	fclose(fptr);
	adminLogin();
}








void uredi() {

	fflush(stdin);
	mangatag(0);
	adminPage();

	FILE* tptr;
	printf("\n\t\t      ***** Proizvodi: Dodatak kolekciji *****       ");

	char id[30];
	int a = 0;



	fflush(stdin);
	printf("\nUnesi ime mange koju bi htjeli urediti: ");
	fgets(id, sizeof(id), stdin);
	gets(id);

	tptr = fopen("temporary.txt", "wb");
	rewind(fptr);                                       //. Koristiti funkcije fseek(), ftell(), rewind(), ovisno o potrebi � neku od njih ako ne sve.

	int flag = 0, flag1 = 0, flag2;

	while (fread(&MANGA, sizeof(MANGA), 1, fptr) == 1)
	{
		if (strcmpi(MANGA.ime, id) == 0)
		{
			flag1++; flag2 = 1;
			fflush(stdin);

			printf("\"%s\" manga postoji u sustavu!\n", id); flag = 10;
			printf("Novo ime(mange): ");
			gets(MANGA.ime);

			printf("Ime autora: ");
			gets(MANGA.autor);

			printf("Zanr: ");
			gets(MANGA.zanr);

			printf("Nova cijena: ");
			scanf("%d", &MANGA.cijena);

			if (flag1 == 1 && flag2 == 1) { fseek(tptr, 0, 0); }

			else { fseek(tptr, (flag1 - 1) * sizeof(MANGA), 0); }

			fwrite(&MANGA, sizeof(MANGA), 1, tptr);

		}
		else
		{
			flag1++; fwrite(&MANGA, sizeof(MANGA), 1, tptr);
		}

	}

	if (flag != 10) { ERROR; puts("Proizvid ne postoji!"); }

	fclose(fptr);
	fclose(tptr);

	remove("mangashop.txt");
	rename("temporary.txt", "mangashop.txt");               //. Koristiti funkcije remove(), rename(), po potrebi implementirati funkciju za kopiranje datoteka.

	fptr = fopen("mangashop.txt", "ab+");

	printf("\nPRITISNITE ENTER KAKO BI SE VRATILI NA PRETHODNU STRANICU... ");

	getch();
	system("cls");


	fclose(fptr);
	adminLogin();
}









void pregled() {


	mangatag(0);
	adminPage();

	printf("\n\t\t     ***** Trenutni odabir: Pregled cijele kolekcije *****     ");
	rewind(fptr);

	char valuta[] = "EUR";

	while (fread(&MANGA, sizeof(MANGA), 1, fptr) == 1)
	{
		printf("\n\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD");
		printf("\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD");
		printf("\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD");
		printf("\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD");
		printf("\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD");
		printf("\nIme mange: ");
		puts(MANGA.ime);

		printf("Autor: ");
		puts(MANGA.autor);

		printf("Zanr: ");
		puts(MANGA.zanr);

		printf("Cijena: ");
		printf("%d ", MANGA.cijena);
		printf("%s\n", valuta);
	}
	printf("\nPRITISNITE ENTER KAKO BI SE VRATILI NA PRETHODNU STRANICU...");
	getch();
	system("cls");
	fclose(fptr);
	adminLogin();
}








void pretraga() {


	int a = 0;
	char id[30];
	char valuta[] = "EUR";

	mangatag(0);
	adminPage();

	fptr = fopen("mangashop.txt", "ab+");
	fflush(stdin);

	printf("\n\t\t     ***** Trenutni odabir: Pretraga dostupnih proizvoda u trgovini *****      ");

	puts("\nIme mange: ");
	gets(MANGA.ime);
	gets(id);



	while (fread(&MANGA, sizeof(MANGA), 1, fptr) == 1)
	{
		if (strcmpi(MANGA.ime, id) == 0) {
			puts("\n-------------------------------------------------\n\t\t PRETRAGA:\n-------------------------------------------------\n");
			printf("\nIme mange: ");
			puts(MANGA.ime);

			printf("Autor: ");
			puts(MANGA.autor);

			printf("Zanr: ");
			puts(MANGA.zanr);

			printf("Cijena: ");
			printf("%d ", MANGA.cijena); a = 1;
			printf("%s\n", valuta);
		}
	}

	if (a != 1) { ERROR; printf("\n%s nije dostupan u trgovini!\n", id); }
	printf("\nPRITISNITE ENTER KAKO BI SE VRATILI NA PRETHODNU STRANICU...");
	getch();
	system("cls");
	fclose(fptr);
	adminLogin();
}









void obrisi() {

	mangatag(0);
	adminPage();
	fflush(stdin);

	FILE* tptr;
	printf("\n\t\t     ***** Trenutni odabir: Brisanje mangi koje vise nisu dostupne u trgovini *****        ");

	char id[30];
	int a = 0;


	fflush(stdin);

	printf("\nUnesite ime mange: ");
	gets(MANGA.ime);
	gets(id);
	id[strcspn(id, "\n")] = '\0';


	tptr = fopen("tempdel.txt", "wb");
	rewind(fptr);

	int flag = 0;
	while (fread(&MANGA, sizeof(MANGA), 1, fptr) == 1)
	{
		if (strcmp(MANGA.ime, id) != 0)
		{
			fwrite(&MANGA, sizeof(MANGA), 1, tptr);
		}
		else if (strcmp(MANGA.ime, id) == 0) {                   //. Za�tita parametara kod svih funkcija.
			puts("\a\nManga uspjesno obrisana!\n");
			flag = 10;
		}
	}

	if (flag != 10) {
		ERROR;
		puts("Proizvod ne postoji u trgovini!");
	}

	fclose(fptr);
	fclose(tptr);

	remove("mangashop.txt");
	rename("tempdel.txt", "mangashop.txt");

	fptr = fopen("mangashop.txt", "ab+");


	printf("\nPRITISNITE ENTER KAKO BI SE VRATILI NA PRETHODNU STRANICU...");

	getch();
	system("cls");
	fclose(fptr);
	adminLogin();
}







int login() {


	fflush(stdin);
	char admin[30];
	static char lozinka[5];
	int a;

	printf("\n\t\t\tUsername: ");
	scanf("%s", &admin);


	if (strcmpi(admin, "admin") == 0 || strcmpi(admin, "rz9wZ9bL") == 0 || strcmpi(admin, "ferit") == 0)
	{
		puts("\n\n\t\t\tIdentitet potvdjen!");
		printf("\n\t\t\tLozinka: \n\t\t\t ");

		for (int i = 0; i < 5; i++) {

			lozinka[i] = getch();

			if (lozinka[i] == '\r') { break; }

			else
				printf("*");
		}

		if (strcmp(lozinka, "12345") == 0) {

			puts("\a\n\n\n\t\t\tULAZ ODOBREN"); getch(); a = 1;
		}

		else {

			puts("\a\n\n\n\t\t\tULAZ ODBIJEN!"); a = 0;
		}
	}

	else { mangatag(0); puts("\n\n\n\t\t\tNEPOZNAT KORISNIK"); a = 0; }

	return a;
}



//funkcije za izgled


void TITLE() {


	system("cls");
	system("color 3");
	printf("\n\n\n\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD");
	printf("\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD");
	printf("\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD");
	printf("\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD");
	printf("\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD");
	printf("\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD");
	printf("\n\n\n\nZAVRSNI PROJEKT\n\n\nONLINE\nMANGA STORE\n\n\n\nNapravila: Julia Penic\n");


	printf("\n\n\n\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD");
	printf("\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD");
	printf("\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD");
	printf("\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD");
	printf("\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD");
	printf("\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\n");
	puts("\nPritisnite tipku za dalje...");
	getch();
}

int mangatag(int bal) {
	system("cls");
	printf("\n=================================================================================================================\n");
	printf("\n\t\tMANGA SHOP\n");
	printf("\t Best manga money can buy!\n");
	printf("\n=================================================================================================================\n");
}
int mangapur(int bal) {
	system("cls");
	printf("\n=================================================================================================================\n");
	printf("\n\t\tMANGA SHOP\n");
	printf("\t Best manga money can buy!\n"); printf("\n\t\t\t\t\t\tTrenutni iznos = %d", bal);
	printf("\n=================================================================================================================\n");
}





void adminPage() {
	printf("\t");
	printf("\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD");
	printf("\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD");
	printf("\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD");
	printf("\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD");
	printf("\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD");
	printf("\n\t\t\t\t\tADMIN PAGE\n");
	printf("\t");
	printf("\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD");
	printf("\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD");
	printf("\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD");
	printf("\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD");
	printf("\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\n");
}



void customerPage() {
	printf("\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD");
	printf("\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD");
	printf("\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD");
	printf("\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD");
	printf("\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD");
	printf("\n\t\t\tCUSTOMER PURCHASE\n");
	printf("\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD");
	printf("\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD");
	printf("\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD");
	printf("\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD");
	printf("\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\n");
}